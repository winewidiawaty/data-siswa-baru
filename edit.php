<?php 

include("config.php");

if( !isset($_GET['id']) ){
	// kalau tidak ada id di query string
	header('Location: list-data.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM datasiswabaru WHERE id=$id";
$query = mysqli_query($db, $sql);
$siswa = mysqli_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
	die("data tidak ditemukan...");
}

?>


<!DOCTYPE html>
<html>
<head>
	<title>Formulir Edit Mahasiswa</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
</head>

<body>
	<header><CENTER>
		<h3>Formulir Edit Mahasiswa</h3>
	</CENTER>
		
	</header>
	
	<form action="proses-edit.php" method="POST">
		<div class="kotak_daftar">
			<p class="tulisan_daftar"></p>
				<fieldset>
			
			  <input type="hidden" name="id" value="<?php echo $siswa['id'] ?>" />

        <p>
            <label for="nama" class="tulisan_daftar">Nama</label>
            <input type="text" class="form_daftar" name="nama" placeholder="nama lengkap" value="<?php echo $siswa['nama'] ?>" />
        </p>
		<p>
			<label for="email" class="tulisan_daftar">Email :</label>
			<input type="text" class="form_daftar" name="email" placeholder="email" value="<?php echo $siswa['email']?>"/>
		</p>
		<p>
			<label for="website" class="tulisan_daftar">Website :</label>
			<input type="text" class="form_daftar" name="website" placeholder="website" value="<?php echo $siswa['website']?>"/>
		</p>
		<p>
			<label for="kelas" class="tulisan_daftar">Kelas :</label>
			<input type="text" class="form_daftar" name="kelas" placeholder="kelas" value="<?php echo $siswa['kelas']?>"/>
		</p>
		<p>
			<label for="alamat" class="tulisan_daftar">Alamat :</label>
			<input type="text" class="form_daftar" name="alamat" placeholder="alamat lengkap" value="<?php echo $siswa['alamat']?>"/>
		</p>
		<p><input type="submit" class="tombol_daftar" value="Simpan" name="simpan"></p>
		</fieldset>
			
		</div>
		
		
	
	</form>
	
	</body>
</html>
